const return_http_500 = require('./errorController')
const fs = require('fs')
const os = require('os')
const path = require('path')
const repo = require('../repository/candidatesFileRepository')

function readAll (req, res) {
    repo.readAll((err, data) => {
        if (err) {
            return_http_500(err, res)
        }
        else {
            res.send(data)
        }
    })
}

function read (req, res) {
    repo.read((err, data) => {
        if (err) {
            return_http_500(err, res)
        }
        else {
            res.send(JSON.parse(data))
        }
    })
}

module.exports = {readAll}
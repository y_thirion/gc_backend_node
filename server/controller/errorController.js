
function return_http_500 (err, res) {
    res.locals.message = err.message
    res.locals.error = err
    res.status(500)
    res.render('error')
}

function return_config_error (err, res) {
    res.locals.message = err.message
    res.locals.error = err
    res.status(500)
    res.render('config_error')
}

module.exports = { return_http_500, return_config_error}
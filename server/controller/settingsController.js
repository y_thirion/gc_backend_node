const fs = require('fs')

const settingsFile = './server/config.json'

function loadSettings (callback) {
 
    fs.readFile(settingsFile, 'utf8', (err, data) => {
        if (! err) {
            global._gcontest.settings = JSON.parse(data)
            console.log(`Settings successfully loaded from file ${settingsFile} : ${JSON.stringify(global._gcontest.settings)}`)
        }
        else {
            console.log(`Unable to load settings from file ${settingsFile}`)
        }

        callback(err)
    })
}

module.exports = {loadSettings}
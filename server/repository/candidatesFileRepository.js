const fs = require('fs')
const os = require('os')
const path = require('path')

function readAll (callback) {
    fs.readFile(path.join(os.homedir(), global._gcontest.settings.candidatesFile), (err, data) => {
        callback(err, JSON.parse(data))
    })   
}

function read (id, callback) {
    readAll((err, data) => {
        // Identify the right one
        console.log(JSON.stringify(data))
    })
}

module.exports = {readAll, read}
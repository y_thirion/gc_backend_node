var createError = require('http-errors')
var express = require('express')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')

var indexRouter = require('./routes/www/indexRouter')
var apiRouter = require('./routes/www/apiRouter')
var usersRouter = require('./routes/api/usersRouter')
var candidatesRouter = require('./routes/api/candidatesRouter')
var settingsController = require('./controller/settingsController')

var app = express()

var err = settingsController.loadSettings((err) => {
  if (err) {
    console.log(`Unable to start server without settings - Server will now stop`)
    global._gcontest.server.close()
  }
  else {
    startServer()
  }
})

function startServer() {
  console.log(`Starting application from ${process.cwd()} on (${app.get('env')}) environment`)
  
  // view engine setup
  app.set('views', path.join(__dirname, 'views'))
  app.set('view engine', 'pug')
  
  app.use(logger('dev'))
  app.use(express.json())
  app.use(express.urlencoded({ extended: false }))
  app.use(cookieParser())
  
  var pub = path.join(process.cwd(), 'public')
  app.use(express.static(pub))
  console.log(`Serving static pages server from ${pub}`)
  
  app.use('/', indexRouter)
  app.use('/api', apiRouter)
  app.use('/api/users', usersRouter) // TODO : wrap into apiRouter
  app.use('/api/candidates', candidatesRouter)  // TODO : wrap into apiRouter
  
  // catch 404 and forward to error handler
  app.use(function(req, res, next) {
    next(createError(404))
  })
  
  // error handler
  app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message
    res.locals.error = req.app.get('env') === 'development' ? err : {}
  
    // render the error page
    res.status(err.status || 500)
  
    res.status === 404 ? res.render('404') : res.render('error')
  })
}

module.exports = app
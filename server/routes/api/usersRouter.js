const express = require('express'),
    // eslint-disable-next-line new-cap
    router = express.Router()

/* GET users listing. */
router.get('/', (req, res, next) => {
    res.send('This is the list of users')
})

module.exports = router

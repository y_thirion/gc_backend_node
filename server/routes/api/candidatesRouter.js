const express = require('express')
const router = express.Router()
const controller = require('../../controller/candidatesController')

/* GET home page. */
router.get('/', function(req, res, next) {

    controller.readAll(req, res);
})

module.exports = router
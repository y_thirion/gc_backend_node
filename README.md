# GContest backend

Backend tier for GContext application

## Launch

To launch the NodeJS backend, type the following command line :

`npm start`

## VScode extensions used

* `node-snippets`
* `node-readme`
* `Node.js Extension Pack`
* `UNOTES - Markdown Notes WYSIWYG`

## Prerequisites

These npm packages must be installed globally prior to use the code :

* eslint
* nodemon

The following npm packages have been used but are not required :

* express-generator